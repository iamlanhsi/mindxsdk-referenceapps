# Copyright(C) 2022. Huawei Technologies Co.,Ltd. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 变量说明
export MX_SDK_path=""# mxVision 安装路径
export Ascend_toolkit_path=""# CANN 安装路径

# MindXSDK 环境变量：
. /${MX_SDK_path}/set_env.sh

# CANN 环境变量：
. /${Ascend_toolkit_path}/set_env.sh

